# [www.open-cuts.org](https://www.open-cuts.org)

[![ci](https://gitlab.com/open-cuts/www.open-cuts.org/badges/master/pipeline.svg)](https://gitlab.com/open-cuts/www.open-cuts.org/)

The [OPEN-CUTS website](https://www.open-cuts.org)! Made using [Gridsome](https://gridsome.org/). You can start a development server by running `npm start`.

## License

Original development by [Johannah Sprinz](https://spri.nz). Copyright (C) 2020-2022 [UBports Foundation](https://ubports.com).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
